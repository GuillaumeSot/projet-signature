<?php

namespace App\Command;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A command that assign the ROLE_MANAGER to a user
 */
class ManagerCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'role:manager';

    public function __construct(UserRepository $userRepository, EntityManagerInterface $em)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->em = $em;
    }

    protected function configure(): void
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription("Upgrade someone's role to ROLE_MANAGER.")

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to upgrade someone's role to ROLE_MANAGER")
            ->addArgument('username', InputArgument::REQUIRED, 'Who do you want to upgrade?');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $usernameInput = $input->getArgument('username');


        $user = $this->userRepository->findOneByUsername($usernameInput);
        $user->setRoles(["ROLE_MANAGER"]);
        $this->em->flush();

        $output->writeln($usernameInput . ' has been upgraded to ROLE_MANAGER !');


        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        return Command::SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;
    }
}
