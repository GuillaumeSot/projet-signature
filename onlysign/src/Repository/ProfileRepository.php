<?php

namespace App\Repository;

use App\Entity\Profile;
use App\Entity\Formation;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Profile|null find($id, $lockMode = null, $lockVersion = null)
 * @method Profile|null findOneBy(array $criteria, array $orderBy = null)
 * @method Profile[]    findAll()
 * @method Profile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Profile::class);
    }

    /**
     * Find an array of Profile by formation_role.
     * 
     * @param string $role
     * @param Formation $formation
     * @return Profile[] Returns an array of Profile objects
     * 
     */
    public function findPersonByRole($role, $formation): array
    {

        $qb = $this->createQueryBuilder('p')
            ->leftJoin('p.user', 'u')
            ->leftJoin('p.formation', 'f')
            ->where('p.formation = ' . $formation)
            ->andWhere('p.formation_role LIKE :role')
            ->setParameter('role', $role)
            ->orderBy('u.lastName', 'ASC');

        $query = $qb->getQuery();

        return $query->execute();
    }



    /**
     * Find Profiles by formation
     * 
     * @param Formation $formation
     * @return Profile[] Returns an array of Profile objects
     * 
     */

    public function findPersonByFormation($formation): array
    {

        $qb = $this->createQueryBuilder('p')
            ->leftJoin('p.user', 'u')
            ->leftJoin('p.formation', 'f')
            ->where('p.formation = ' . $formation);

        $query = $qb->getQuery();

        return $query->execute();
    }

    // automatically knows to select Products
    // the "p" is an alias you'll use in the rest of the query


    // $qb = $this->createQueryBuilder('p')
    //     ->where('p.price > :price')
    //     ->setParameter('price', $price)
    //     ->orderBy('p.price', 'ASC');

    // if (!$includeUnavailableProducts) {
    //     $qb->andWhere('p.available = TRUE');
    // }

    // $query = $qb->getQuery();

    // return $query->execute();



    // to get just one result:
    // $product = $query->setMaxResults(1)->getOneOrNullResult();






    /*
    public function findOneBySomeField($value): ?Profile
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
