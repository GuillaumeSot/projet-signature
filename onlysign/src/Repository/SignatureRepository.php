<?php

namespace App\Repository;

use App\Entity\Signature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Signature|null find($id, $lockMode = null, $lockVersion = null)
 * @method Signature|null findOneBy(array $criteria, array $orderBy = null)
 * @method Signature[]    findAll()
 * @method Signature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SignatureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Signature::class);
    }


    /**
     * Find signatures objects by session
     * 
     * @param Session $session
     * @return Signature
     */
    public function findSignatureBySession($session): array
    {

        $qb = $this->createQueryBuilder('s')
            ->leftJoin('s.session', 'session')
            ->where('s.session = ' . $session);

        $query = $qb->getQuery();

        return $query->execute();
    }


    /*
    public function findOneBySomeField($value): ?Signature
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
