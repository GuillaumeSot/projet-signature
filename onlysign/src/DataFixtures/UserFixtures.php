<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Faker;


/**
 * Generate random 20 Users with the ROLE_USER and 3 Users with the ROLE_MANAGER
 */
class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        for ($count = 0; $count < 20; $count++) {
            $user = new User();
            $user->setLastName($faker->lastName);
            $user->setFirstName($faker->firstName);
            $user->setEmail($faker->email);
            $user->setPhone($faker->phoneNumber);
            $user->setUsername($faker->userName);
            $user->setPassword($faker->password);
            $user->setRoles(["ROLE_USER"]);
            $manager->persist($user);
        }
        for ($count = 0; $count < 3; $count++) {
            $user = new User();
            $user->setLastName($faker->lastName);
            $user->setFirstName($faker->firstName);
            $user->setEmail($faker->email);
            $user->setPhone($faker->phoneNumber);
            $user->setUsername($faker->userName);
            $user->setPassword($faker->password);
            $user->setRoles(["ROLE_MANAGER"]);
            $manager->persist($user);
        }


        $manager->flush();
    }
}
