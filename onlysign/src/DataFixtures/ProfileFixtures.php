<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\Profile;
use App\Entity\User;
use App\Entity\Formation;
use App\DataFixtures\UserFixtures;
use App\DataFixtures\FormationFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


/**
 * Generate fake profiles associating Users with Formations
 */
class ProfileFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {


        $repository = $manager->getRepository(User::class);
        $users = $repository->findAll();

        $datas = $manager->getRepository(Formation::class);
        $formations = $datas->findAll();
        $faker = Faker\Factory::create();

        $formationRoles = [
            1 => 'Student',
            2 => 'Teacher',
            3 => 'Chief'
        ];

        foreach ($users as $user) {
            $profile = new Profile();
            $profile->setUser($user);
            $profile->setFormation($formations[random_int(0, 4)]);
            $profile->setFormationRole($formationRoles[random_int(1, 3)]);
            $profile->setSubscriptionDate($faker->dateTime($max = 'now', $timezone = null));
            $manager->persist($profile);
        }

        $manager->flush();
    }

    /**
     * Set the Fixtures that need to be loaded before this one
     *
     * @return void
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            FormationFixtures::class,
        ];
    }
}
