<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\Formation;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use phpDocumentor\Reflection\Types\Null_;

/**
 * Generate fake random formations
 */
class FormationFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        for ($count = 0; $count < 5; $count++) {
            $formation = new Formation();
            $formation->setTitle($faker->company);
            $formation->setLocation($faker->city);
            $formation->setBeginDate($faker->dateTime($max = 'now', $timezone = null));
            $formation->setEndDate($faker->dateTime($max = 'now', $timezone = null));
            $manager->persist($formation);
        }
        $manager->flush();
    }
}
