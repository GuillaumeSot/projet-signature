<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class ProfileAccessVoter extends Voter
{
    const UPDATE = 'updateProfile';
    const SHOW = 'showProfile';

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [
            self::UPDATE,
            self::SHOW
        ]);
    }
    
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }


    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case self::UPDATE:
            case self::SHOW:
                return $this->isOwnProfile($user, $subject);
        }

        return false;
    }

    private function isOwnProfile($user, User $subject): bool {
        return $user->getId() === $subject->getId();
    }
}
