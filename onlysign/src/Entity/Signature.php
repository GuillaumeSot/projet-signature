<?php

namespace App\Entity;

use App\Repository\SignatureRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SignatureRepository::class)
 */
class Signature
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\Type("bool")
     */
    private $presence;

    /**
     * @ORM\ManyToOne(targetEntity=Session::class, inversedBy="signatures")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $session;

    /**
     * @ORM\ManyToOne(targetEntity=Profile::class, inversedBy="signatures")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $profile;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPresence(): ?bool
    {
        return $this->presence;
    }

    public function setPresence(bool $presence): self
    {
        $this->presence = $presence;

        return $this;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }
}
