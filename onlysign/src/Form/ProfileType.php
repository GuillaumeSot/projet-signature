<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Profile;
use App\Entity\Formation;
use App\Form\FormationType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subscription_date', DateType::class, [
                'widget' => 'choice',
                'data' => new \DateTime('now')
            ])
            // Create a field using a custom query rendering all formations ordered by their title
            ->add('formation', EntityType::class, [
                "class" => Formation::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('f')
                        ->orderBy('f.title', 'ASC');
                },
                'choice_label' => 'title',
            ])
            // Create a field with 3 choices of indicative roles
            ->add('role', ChoiceType::class, [
                'choices' => [
                    'Student' => 'Student',
                    'Teacher' => 'Teacher',
                    'Chief' => 'Chief',
                ]
            ])
            // Create a field using a custom query rendering all users ordered by their username
            // The "multiple" option allows you to pick many users
            ->add('user', EntityType::class, [
                "class" => User::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.username', 'ASC');
                },
                'choice_label' => 'username',
                "multiple" => true,
            ]);
    }
}
