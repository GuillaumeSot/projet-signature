<?php

namespace App\Form;

use App\Entity\Formation;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class SessionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('formation', EntityType::class, [
                'class' => Formation::class,
                'choice_label' => 'title',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('f')
                        ->orderBy('f.title', 'ASC');
                },
            ])
            ->add('begin_date', DateType::class, [
                'data' => new \DateTime('now')
            ])
            ->add('begin_hour', TimeType::class)
            ->add('end_hour', TimeType::class)
            ->add('end_date', DateType::class, [
                'data' => new \DateTime('now')
            ])
            ->add('exceptions', CollectionType::class, [
                'entry_type' => ExceptionType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'label' => false,
            ]);
    }
}
