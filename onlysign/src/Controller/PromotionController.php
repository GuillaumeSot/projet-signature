<?php

namespace App\Controller;

use App\Entity\Profile;
use App\Repository\ProfileRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PromotionController extends AbstractController
{
    /**
     * Shows you the list of persons in your formation[0], sorted by formation role
     * 
     * @Route("/promotion", name="promotion")
     */
    public function index(EntityManagerInterface $manager): Response
    {
        /** @var ProfileRepository $repository **/
        $repository = $manager->getRepository(Profile::class);
        $students = "Student";
        $teachers = "Teacher";
        $chief = "Chief";

        $formation = $this->getUser()->getProfiles();
        $formation = $formation[0]->getFormation()->getId();
        $studentsList = $repository->findPersonByRole($students, $formation);
        $teachersList = $repository->findPersonByRole($teachers, $formation);
        $chiefsList = $repository->findPersonByRole($chief, $formation);




        return $this->render('promotion/index.html.twig', [
            'studentsList' => $studentsList,
            'teachersList' => $teachersList,
            'chiefsList' => $chiefsList,

        ]);
    }
}
