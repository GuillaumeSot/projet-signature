<?php

namespace App\Controller;

use App\Entity\Signature;
use App\Form\Signature1Type;
use App\Repository\SignatureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/signature")
 */
class SignatureController extends AbstractController
{
    /**
     * Shows you the list of all signatures
     * 
     * @Route("/", name="signature_index", methods={"GET"})
     */
    public function index(SignatureRepository $signatureRepository): Response
    {
        return $this->render('signature/index.html.twig', [
            'signatures' => $signatureRepository->findAll(),
        ]);
    }

    /**
     * Shows you the informations of the Signature you clicked on
     * 
     * @Route("/{id}", name="signature_show", methods={"GET"})
     */
    public function show(Signature $signature): Response
    {
        return $this->render('signature/show.html.twig', [
            'signature' => $signature,
        ]);
    }

    /**
     * Edit the Signature you clicked on and allows you to set the presence of someone to true or false
     * 
     * @Route("/{id}/edit", name="signature_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Signature $signature): Response
    {
        $form = $this->createForm(Signature1Type::class, $signature);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('signature_index');
        }

        return $this->render('signature/edit.html.twig', [
            'signature' => $signature,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Delete the Signature you clicked on
     * 
     * @Route("/{id}", name="signature_delete", methods={"POST"})
     */
    public function delete(Request $request, Signature $signature): Response
    {
        if ($this->isCsrfTokenValid('delete' . $signature->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($signature);
            $entityManager->flush();
        }

        return $this->redirectToRoute('signature_index');
    }
}
