<?php

namespace App\Controller;

use DateTime;
use DateInterval;
use App\Entity\Profile;
use App\Entity\Session;
use App\Entity\Exception;
use App\Entity\Signature;
use App\Form\SessionType;
use App\Form\SignatureType;
use App\Form\EditSessionType;
use App\Repository\UserRepository;
use App\Repository\ProfileRepository;
use App\Repository\SessionRepository;
use App\Repository\SignatureRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/session")
 */
class SessionController extends AbstractController
{
    /**
     * Shows you the list of current and incomming sessions of your formation[0]
     * 
     * @Route("/", name="session_index", methods={"GET"})
     */
    public function index(Request $request, SessionRepository $sessionRepository, PaginatorInterface $paginator): Response
    {
        // get the current date
        $currentDate = new DateTime("now");

        // get the formation[0] of the connected user
        $formation = $this->getUser()->getProfiles();
        $formation = $formation[0]->getFormation()->getId();

        // get every sessions of the formation[0] of the connected user
        $mySessions = $sessionRepository->findBy(
            ["formation" => $formation],
            ["begin_date" => "ASC"]
        );

        // Sort current and incomming sessions, without passed ones, and push them into an array
        $tableSession = [];
        foreach ($mySessions as $mySession) {
            if ($currentDate  <= $mySession->getEndDate()) {
                array_push($tableSession, $mySession);
            }
        };

        // Paginate the sessions with 10 max results by page, starting by the page 1
        $sessions = $paginator->paginate(
            $tableSession,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('session/index.html.twig', [
            'sessions' => $sessions,
            'currentDate' => $currentDate,

        ]);
    }

    /**
     * Create session(s) getting a formation, a begin_date, an end_date, a begin_hour and an end_hour. You can set exceptions periods that will not be pushed in the database
     * 
     * @Route("/new", name="session_new")
     * 
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        // Only managers and admins can access this page
        $this->denyAccessUnlessGranted("ROLE_MANAGER");


        $form = $this->createForm(SessionType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Get all datas from the submitted form
            $data = $form->getData();
            $beginDate = $data["begin_date"];
            $beginHour = $data["begin_hour"]->format('H:i');
            $endHour = $data["end_hour"]->format('H:i');
            $endDate = $data["end_date"];
            $formation = $data["formation"];
            $exceptions = $data["exceptions"];

            // Create new date intervals, which allows you to jump periods
            $plusOneDay = new DateInterval("P1D");
            $jumpWeekEnd = new DateInterval("P2D");

            // Initialize an empty collection of Exception objects
            $exceptionsCollection = new ArrayCollection();

            // Get the exceptions array from the form
            // Create a new Exception object for each iterations of this array
            // Push each objects in the exceptionCollection[] 
            foreach ($exceptions as $i) {
                $exception = new Exception();

                $beginException = $i->getBeginDate();
                $endException = $i->getEndDate();

                $exception->setBeginDate($beginException);
                $exception->setEndDate($endException);
                $exceptionsCollection->add($exception);
            }

            // Get the begin_date and end_date you entered in the form
            // While the begin_date is inferior or equal to the end_date, stay in the loop
            while ($beginDate <= $endDate) {

                // Concat the begin_date and the form's begin_hour to make a conform datetime format
                // Assign it to a variable named beginTimeSlot
                $beginTimeSlot = new DateTime($beginDate->format('Y-m-d') . ' ' . $beginHour);
                // Same for the begin_date and end_hour into endTimeSlot
                $endTimeSlot = new DateTime($beginDate->format('Y-m-d') . ' ' . $endHour);
                // Get the begin_date day at format 'D' -> ex: 'Monday', 'Sunday', etc...
                $day = $beginDate->format('D');

                // Create a new Session object
                $newSession = new Session();
                // Assign beginTimeSlot and endTimeSlot to the formation you picked in the form
                $newSession->setBeginDate($beginTimeSlot);
                $newSession->setEndDate($endTimeSlot);
                $newSession->setFormation($formation);

                // Create Exceptions objects
                foreach ($exceptionsCollection as $exception) {

                    $begin = $exception->getBeginDate();
                    $end = $exception->getEndDate();
                    // Get the number of days between the beginning and the end of your exception period
                    $dateDiff = $end->diff($begin)->format("%a");
                    // Create a new DateInterval of this number of days
                    $numbersOfDaysToJump = "P" . $dateDiff . "D";
                    $jumpException = new DateInterval($numbersOfDaysToJump);
                    // If beginTimeSlot is equal to the beginning date of the exception period
                    // Jump this exception
                    if ($beginTimeSlot == $begin) {
                        $beginDate->add($jumpException);
                    }
                };

                // If the beginDate is Saturday, jump 2 days (the week-end)
                if ($day == "Sat") {
                    $beginDate->add($jumpWeekEnd);
                }
                // Add one day to beginDate
                $beginDate->add($plusOneDay);

                $entityManager->persist($newSession);
            }

            // Push every Session objects in the database
            $entityManager->flush();

            return $this->redirectToRoute('session_index');
        }


        return $this->render('session/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    /**
     * Generate a 5 numbers code at the begining of the current session, and allows you to sign
     * 
     * @Route("/{id}/gencode", name="generate_code")
     */
    public function generateCode(Session $currentSession, EntityManagerInterface $entityManager, Request $request, ProfileRepository $pRepository, SignatureRepository $sRepository): Response
    {
        // Get the current date
        $currentDate = new DateTime("now");

        // Get the ID of the formation[0] of the connected user
        $profiles = $this->getUser()->getProfiles();
        $currentProfile = $profiles[0];
        $currentFormation = $currentProfile->getFormation()->getId();

        // Get all profiles in this formation
        $profilesInMyFormation = $pRepository->findPersonByFormation($currentFormation);

        $entityManager = $this->getDoctrine()->getManager();

        // Get all the sessions of this formation
        $sessions = $entityManager->getRepository(Session::class)->findAll();

        // Get all the Signature objects of the session you clicked on
        $mySignature = $sRepository->findSignatureBySession($currentSession->getId());

        // Get the begin_date and end_date of this session
        $begin = $currentSession->getBeginDate();
        $end = $currentSession->getEndDate();

        // Generate a code if :
        // The current session_begin_date is superior or equal to the current date
        // The current session_end_date is superior or equal to the current date
        // There is no code set in this session
        // WARNING : only MANAGER and ADMIN roles can see the generated code
        if ($currentDate >= $begin && $currentDate <= $end && $currentSession->getCode() == null) {
            // Generate the code randomly and assign it to this Session
            $generatecode = random_int(10000, 99999);
            $currentSession->setCode($generatecode);

            // Initialize Session objects for each Profile in this Formation with presence on false
            foreach ($profilesInMyFormation as $p) {
                $generateSignatureEntry = new Signature();
                $generateSignatureEntry->setPresence(false)
                    ->setSession($currentSession)
                    ->setProfile($p);
                $entityManager->persist($generateSignatureEntry);
            }
            // Push these Signature objects in the database
            $entityManager->flush();
        };


        $form = $this->createForm(SignatureType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Get all datas from the submitted form
            $data = $form->getData();
            $inputCode = $data["code"];
            $presence = $data["presence"];

            $sessionCode = $currentSession->getCode();
            // If the code of the session is equal to the code you submitted
            if ($sessionCode == $inputCode && $presence == true) {
                // Set the presence to true
                $signature = $mySignature[0]->setPresence($presence);

                $entityManager->persist($signature);
                // Modify the signature of the connected user to true
                $entityManager->flush();

                return $this->redirectToRoute('session_success');
            }
            // If it's not the same code, shows you a flash message saying you did not entered the right code
            else {
                $this->addFlash("message", "Wrong session code or sign box unchecked !");
            }
        }

        // If the end_date of the session is superior to the current date, does not show the session
        if ($currentDate <= $end) {
            return $this->render('session/sign.html.twig', [
                // 'code' => $generatecode, 'sessions' => $sessionRepository->findAll(), 'formation' => $user
                'session' => $currentSession,
                'form' => $form->createView(),
            ]);
        }
        // Else, deny the access to users (students)
        else {
            $this->denyAccessUnlessGranted("ROLE_MANAGER");
            return $this->redirectToRoute('bundles/TwingBundle/Exception/error403.html.twig', [
                // 'code' => $generatecode, 'sessions' => $sessionRepository->findAll(), 'formation' => $user
                'session' => $currentSession,
                'form' => $form->createView(),
                'sessions' => $sessions
            ]);
        }
    }

    /**
     * Show you a screen with a simple success message
     * 
     * @Route("/success", name="session_success" , methods={"GET"})
     */
    public function success(Session $session = null): Response
    {
        return $this->render('session/success.html.twig', [
            'session' => $session,
        ]);
    }

    /**
     * Show you the session you clicked on
     * 
     * @Route("/{id}", name="session_show", methods={"GET"})
     */
    public function show(Session $session): Response
    {
        $this->denyAccessUnlessGranted("ROLE_MANAGER");
        return $this->render('session/show.html.twig', [
            'session' => $session,
        ]);
    }



    /**
     * Edit the session you clicked on
     * 
     * @Route("/{id}/edit", name="session_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Session $session): Response
    {
        $this->denyAccessUnlessGranted("ROLE_MANAGER");
        $form = $this->createForm(EditSessionType::class, $session);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('session_index');
        }

        return $this->render('session/edit.html.twig', [
            'session' => $session,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Delete the session you clicked on
     * 
     * @Route("/{id}", name="session_delete", methods={"POST"})
     */
    public function delete(Request $request, Session $session): Response
    {
        $this->denyAccessUnlessGranted("ROLE_MANAGER");
        if ($this->isCsrfTokenValid('delete' . $session->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($session);
            $entityManager->flush();
        }

        return $this->redirectToRoute('session_index');
    }
}
