<?php

namespace App\Controller;

use App\Entity\Profile;
use App\Form\ProfileType;
use App\Repository\ProfileRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/profile")
 */
class ProfileController extends AbstractController
{
    /**
     * Shows you the list of all profiles. A profile is an association of 1 User and 1 Formation
     * 
     * @Route("/", name="profile_index", methods={"GET"})
     */
    public function index(ProfileRepository $profileRepository): Response
    {
        return $this->render('profile/index.html.twig', [
            'profiles' => $profileRepository->findAll(),
        ]);
    }

    /**
     * Add one or many User(s) to an existing formation, choosing his/their formation role and subscription date
     * 
     * @Route("/new", name="profile_new", methods={"GET","POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ProfileType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Get datas from the submitted form
            $data = $form->getData();

            $subscriptionDate = $data['subscription_date'];
            $formation = $data['formation'];
            $users = $data['user'];
            $role = $data['role'];

            // Add as many users you picked to the formation you choosed with a formation_role and a subscription_date
            foreach ($users as $user) {
                $profile = new Profile();
                $profile->setUser($user)
                    ->setFormation($formation)
                    ->setFormationRole($role)
                    ->setSubscriptionDate($subscriptionDate);
                $entityManager->persist($profile);
            }
            $entityManager->flush();

            return $this->redirectToRoute('profile_index');
        }

        return $this->render('profile/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Shows you a profile (associtiation of 1 user and 1 formation)
     * 
     * @Route("/{id}", name="profile_show", methods={"GET"})
     */
    public function show(Profile $profile): Response
    {
        return $this->render('profile/show.html.twig', [
            'profile' => $profile,
        ]);
    }

    /**
     * Edit a profile (associtiation of 1 user and 1 formation)
     * 
     * @Route("/{id}/edit", name="profile_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Profile $profile): Response
    {
        $form = $this->createForm(ProfileType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profile_index');
        }

        return $this->render('profile/edit.html.twig', [
            'profile' => $profile,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Delete a profile (associtiation of 1 user and 1 formation)
     * 
     * @Route("/{id}", name="profile_delete", methods={"POST"})
     */
    public function delete(Request $request, Profile $profile): Response
    {
        if ($this->isCsrfTokenValid('delete' . $profile->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($profile);
            $entityManager->flush();
        }

        return $this->redirectToRoute('profile_index');
    }
}
