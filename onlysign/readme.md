## To install our project, follow these instructions: 

- run : composer install

- run : npm install

- run : npm run build

- run : bin/console server:start OR php -S localhost:8001 -t public
**It will run the project on your localhost:8001 port !**

- run : sudo npm install -g maildev
**It install the Mail environment on the 1025 port of your computer**

- run : maildev --hide-extensions STARTTLS
**It runs the maildev server**
**You can access the mailbox on the url: http://localhost:1080**

## To change someone's role, run :

- bin/console role:admin **username**
- bin/console role:manager **username**
- bin/console role:user **username**

# For more informations, check the wiki in ./wiki/index.html

- Just open it with a browser